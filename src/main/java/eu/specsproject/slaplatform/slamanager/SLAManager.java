/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
*/

package eu.specsproject.slaplatform.slamanager;

import java.util.List;

import eu.specsproject.slaplatform.slamanager.entities.Annotation;
import eu.specsproject.slaplatform.slamanager.entities.Lock;
import eu.specsproject.slaplatform.slamanager.entities.SLADocument;
import eu.specsproject.slaplatform.slamanager.entities.SLAIdentifier;
import eu.specsproject.slaplatform.slamanager.entities.SLASTATE;
import eu.specsproject.slaplatform.slamanager.util.Pair;

/**
 *  The SLA_Manager permits to manage the persistence and the lifecycle of SLAs.
 *  An actual instance can be obtained by using the {@link  SLAManagerFactory}.
 *  
 * 
 * @author Mauro Turtur SPECS  CeRICT
 * 
 */
public interface SLAManager {

    /**
     * Create and persist a SLA into the SLA Platform.
     * The SLA has to be not null, well formed and valid (see {@link SLADocument}), otherwise an {@link IllegalArgumentException} will be thrown.
     * The SLA will be put in the {@link SLASTATE#PENDING} state. 
     * If no errors occur the state automatically evolves into {@link SLASTATE#NEGOTIATING}, in {@link SLASTATE#REJECTED} otherwise. 
     * 
     * @param sla - A valid and well formed SLA to persist.
     * @return an opaque object that identifies the created SLA, intended to be used to perform further operation on it.
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
     */
    public SLAIdentifier create(SLADocument sla);
    
    
    /**
     * Retrieve a SLA from the persistence.
     * An {@link IllegalArgumentException} will be thrown in case of a null or not valid identifier is used as argument.
     * 
     * @param id  a valid and not null SLA identifier.
     * @return the persisted SLA
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
     */
    public SLADocument retrieve(SLAIdentifier id);
    

    public String retrieveCustomerFromSla(SLAIdentifier id);
    
    public void remove(SLAIdentifier id);
    
    /**
     * Retrieve a SLA from the persistence and lock it in order to perform an update.
     * The lock can be acquired only in  {@link SLASTATE#PENDING} {@link SLASTATE#NEGOTIATING} or {@link SLASTATE#RENEGOTIATING state}, otherwise
     * an {@link IllegalStateException} will be thrown.
     * If the lock cannot be acquired (i.e., the SLA is already locked), the returned Lock Object will be null.
     * The Lock is an opaque object that must be used in order to perform further operations on the (locked) SLA.
     * An {@link IllegalArgumentException} will be thrown in case of a null or not valid identifier is used as argument.
     * 
     * @param id  a valid and not null SLA identifier.
     * @return a {@link Pair}  Object, which {@link Pair#obj1} is the retrieved SLA and {@link Pair#obj2} is the Lock. {@link Pair#obj2}can be null if Lock is not acquired.
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
     * @throws IllegalStateException in case of SLA is not in  {@link SLASTATE#PENDING} {@link SLASTATE#NEGOTIATING} or {@link SLASTATE#RENEGOTIATING} state. 
     */
    Pair<SLADocument, Lock> retrieveAndLock(SLAIdentifier id);
    
    
    
    /**
     * 
     * Evolve the SLA status from {@link SLASTATE#SIGNED} to {@link SLASTATE#OBSERVED}. 
     * 
     * @param id  a valid and not null SLA identifier.
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
     * @throws IllegalStateException in case of SLA is not in  {@link SLASTATE#SIGNED} or {@link SLASTATE#IMPLEMENTING} or {@link SLASTATE#REMEDIATING}. 
     */
    public void observe(SLAIdentifier id);
    
    /**
     * 
     * Evolve the SLA status from {@link SLASTATE#SIGNED}  to {@link SLASTATE#IMPLEMENTING}. 
     * 
     * @param id  a valid and not null SLA identifier.
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
     * @throws IllegalStateException in case of SLA is not in  {@link SLASTATE#SIGNED}. 
     */
    public void implement(SLAIdentifier id);
    
    
    /**
     * 
     * Permit to send a termination request to the SLA.
     * It causes the status to evolve to {@link SLASTATE#TERMINATING}.
     * If no internal error occur, the Status automatically evolves to {@link SLASTATE#TERMINATED}. 
     * 
     * @param id  a valid and not null SLA identifier.
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
     * @throws IllegalStateException in case of SLA is not in {@link SLASTATE#OBSERVED} or {@link SLASTATE#PROACTIVE_REDRESSING} or {@link SLASTATE#REMEDIATING} or {@link SLASTATE#NEGOTIATING} or {@link SLASTATE#RENEGOTIATING}. 
     */
    public void terminate(SLAIdentifier id);
    
    
    /**
     * 
     * Permit to complete a SLA in {@link SLASTATE#OBSERVED} state.
     * @param id  a valid and not null SLA identifier.
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
     * @throws IllegalStateException in case of SLA is not in {@link SLASTATE#OBSERVED}. 
     */
    public void complete(SLAIdentifier id);
    
    
    /**
     * 
     * Retrieve the SLA current state.
     *
     * @param id  a valid and not null SLA identifier.
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
     * @return Actual {@link SLASTATE}
     */
    public String getState(SLAIdentifier id);
    

    /**
     * Permit to evolve the SLA status from  {@link SLASTATE#NEGOTIATING} or {@link SLASTATE#RENEGOTIATING} to {@link SLASTATE#SIGNED}. 
     * The sla argument is optional and can be null. Otherwise, if a SLA has been specified, the operation can be invoked only after a proper Lock has been acquired.
     *  After operation successful execution, the SLA will be unlocked.
     * 
     * @param id  a valid and not null SLA identifier.
     * @param sla  the new SLA to be signed. can be null.
     * @param lock  the acquired lock, is mandatory if a sla is specified, it should be null otherwise.
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed, or a SLA has been specified but lock is null or not valid.
     * @throws IllegalStateException in case of SLA is not in {@link SLASTATE#NEGOTIATING} or {@link SLASTATE#RENEGOTIATING}; the SLA was not locked and a sla argument has been specified.
     */
    public void sign(SLAIdentifier id, SLADocument sla,Lock lock );
    
    /**
     * Permit to evolve the SLA status from  {@link SLASTATE#PENDING} to {@link SLASTATE#NEGOTIATING}. 
     * The sla argument is optional and can be null. Otherwise, if a SLA has been specified, the operation can be invoked only after a proper Lock has been acquired.
     *  After operation successful execution, the SLA will be unlocked.
     * 
     * @param id  a valid and not null SLA identifier.
     * @param sla  the new SLA to be signed by the user. can be null.
     * @param lock  the acquired lock, is mandatory if a sla is specified, it should be null otherwise.
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed, or a SLA has been specified but lock is null or not valid.
     * @throws IllegalStateException in case of SLA is not in {@link SLASTATE#PENDING}; the SLA was not locked and a sla argument has been specified.
     */
    public void userSign(SLAIdentifier id, SLADocument sla, Lock lock);   

    	
    /**
     * The operation permits to  modify a SLA document. 
     * The operation will succeed only in  {@link SLASTATE#PENDING} {@link SLASTATE#NEGOTIATING} or {@link SLASTATE#RENEGOTIATING} state and requires a Lock acquired by {@link SLAManager#retrieveAndLock(SLAIdentifier)}.
     *
     * @param id  a valid and not null SLA identifier.
     * @param sla  the new SLA.
     * @param lock  the acquired lock, cannot be null.
     * @throws IllegalArgumentException in case of id and/or SLA and/or lock arguments are null.
     * @throws IllegalStateException in case of SLA is not in {@link SLASTATE#PENDING} {@link SLASTATE#NEGOTIATING} or {@link SLASTATE#RENEGOTIATING}.
     */
    public void update(SLAIdentifier id, SLADocument sla, Lock lock);
    
    /**
     * 
     * Permit to sign an Alert on the SLA in {@link SLASTATE#OBSERVED} state.
     * @param id  a valid and not null SLA identifier.
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
     * @throws IllegalStateException in case of SLA is not in {@link SLASTATE#OBSERVED}. 
     */
    public void signalAlert(SLAIdentifier id);
    
    /**
     * 
     * Permit to sign a Violation on the SLA in {@link SLASTATE#OBSERVED} state.
     * @param id  a valid and not null SLA identifier.
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
     * @throws IllegalStateException in case of SLA is not in {@link SLASTATE#OBSERVED}. 
     */
    public void signalViolation(SLAIdentifier id);
    
    /**
     * 
     * Permit to reNegotiate an Alert on the SLA in {@link SLASTATE#OBSERVED} or {@link SLASTATE#ALERTED} or {@link SLASTATE#VIOLATED} state.
     * @param id  a valid and not null SLA identifier.
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
     * @throws IllegalStateException in case of SLA is not in {@link SLASTATE#OBSERVED} or {@link SLASTATE#ALERTED} or {@link SLASTATE#VIOLATED}. 
     */
    public void reNegotiate(SLAIdentifier id);
    
    /**
     * 
     * Permit to remediate an Violation on the SLA in {@link SLASTATE#VIOLATED} state.
     * @param id  a valid and not null SLA identifier.
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
     * @throws IllegalStateException in case of SLA is not in {@link SLASTATE#VIOLATED}. 
     */
    public void remediate(SLAIdentifier id);
    
    /**
     * 
     * Permit to redress an Alert on the SLA in {@link SLASTATE#ALERTED} state.
     * @param id  a valid and not null SLA identifier.
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
     * @throws IllegalStateException in case of SLA is not in {@link SLASTATE#VIOLATED}. 
     */
    public void redress(SLAIdentifier id);
    
    /**
     * 
     * Permit to unlock a SLA witch had previously locked.
     * @param id a valid and not null SLA identifier.
     * @param lock a valid and not null Lock previously added on SLA.
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
     * @throws IllegalArgumentException in case of Lock argument is null or not valid. 
     */
    public void unlock(SLAIdentifier id,Lock lock);
    
    /**
     * 
     * Set an annotation to the given SLA.
     * An {@link IllegalArgumentException} will be thrown in case of a not valid SLA identifier or a null {@link Annotation}.
     * 
     * @param id  a valid and not null SLA identifier.
     * @param annotation  a not null {@link Annotation} Object
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed and/or annotation is null. 
     */
    public void annotate(SLAIdentifier id,Annotation annotation);
    
    /**
     * 
     * Retrieve the {@link List} of {@link Annotation} for a given SLA.
     * 
     * 
     * @param id  a valid and not null SLA identifier.
     * @return the {@link Annotation} list for the given SLA, may be a 0length list.
     * @throws IllegalArgumentException in case of sla argument is null, not valid nor well formed.
     */
    public List<Annotation> getAnnotations(SLAIdentifier id);
    
    /**
     * 
     * Update the annotation for a given SLA.
     * 
     * 
     * @param id  a valid and not null SLA identifier.
     * @param annId  a valid and not null Annotation identifier.
     * @param ann  a valid and not null Annotation.
     * @throws IllegalArgumentException in case of sla identifier argument is null, not valid or not well formed.
     */
    public void updateAnnotation(SLAIdentifier id,String annId,Annotation ann);

    /**
     * Search SLAs
     * 
     * @return a list of identifiers, may be empty.
     */
    public List<SLAIdentifier> search(String state, String customer, int start, int stop);
    
    
    

}
