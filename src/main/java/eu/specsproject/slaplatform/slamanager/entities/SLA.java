/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
*/

package eu.specsproject.slaplatform.slamanager.entities;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;

import org.eclipse.persistence.nosql.annotations.DataFormatType;
import org.eclipse.persistence.nosql.annotations.Field;
import org.eclipse.persistence.nosql.annotations.NoSql;

//@Entity
//@NoSql(dataFormat=DataFormatType.MAPPED)
public class SLA implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 310273361719993272L;

    //@Id
    //@GeneratedValue
    //@Field(name="_id")
    private String id;
    
    //@Basic(optional = false)
    //@Temporal(javax.persistence.TemporalType.DATE)
    private Date created;
    
    //@Basic(optional = false)
    //@Temporal(javax.persistence.TemporalType.DATE)
    private 
    Date updated;
    
    //@Basic(optional = false)
    private 
    String state;
    
    //@Basic(optional = false)
    //@Embedded
    private 
    SLADocument doc;
    
    //@Basic(optional = false)
    private 
    String customer;

    //@Basic(fetch=FetchType.LAZY)
    private
    List<Annotation> annotations = new ArrayList<Annotation>();
    
    //@Basic(optional = false)
    //@Embedded
    private
    Lock lock;

    public SLA(){
        created = updated = new Date();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public SLADocument getDoc() {
        return doc;
    }

    public void setDoc(SLADocument doc) {
        this.doc = doc;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public List<Annotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<Annotation> annotations) {
        this.annotations = annotations;
    }

    public Lock getLock() {
        return lock;
    }

    public void setLock(Lock lock) {
        this.lock = lock;
    }


}