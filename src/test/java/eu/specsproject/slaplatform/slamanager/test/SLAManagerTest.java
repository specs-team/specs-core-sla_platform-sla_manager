package eu.specsproject.slaplatform.slamanager.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import eu.specsproject.slaplatform.slamanager.SLAManager;
import eu.specsproject.slaplatform.slamanager.SLAManagerFactory;
import eu.specsproject.slaplatform.slamanager.entities.Annotation;
import eu.specsproject.slaplatform.slamanager.entities.Collection;
import eu.specsproject.slaplatform.slamanager.entities.Item;
import eu.specsproject.slaplatform.slamanager.entities.Lock;
import eu.specsproject.slaplatform.slamanager.entities.SLADocument;
import eu.specsproject.slaplatform.slamanager.entities.SLAIdentifier;
import eu.specsproject.slaplatform.slamanager.entities.SLASTATE;
import eu.specsproject.slaplatform.slamanager.entities.SLASTATEenum;
import eu.specsproject.slaplatform.slamanager.internal.EUSLAMangerSQLJPA;
import eu.specsproject.slaplatform.slamanager.internal.marshalling.MarshallingInterface;
import eu.specsproject.slaplatform.slamanager.internal.marshalling.implementation.JSONentityBuilder;
import eu.specsproject.slaplatform.slamanager.internal.marshalling.implementation.JSONmarshaller;
import eu.specsproject.slaplatform.slamanager.internal.marshalling.implementation.XMLentityBuilder;
import eu.specsproject.slaplatform.slamanager.internal.marshalling.implementation.XMLmarshaller;

public class SLAManagerTest {

    private static SLAManager manager;
    private static SLAIdentifier id;
    private static EUSLAMangerSQLJPA ems;
    /*
     * Counter variable is necessary to take into account the number of signed SLAs.
     */
    private static int counter =0;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        
        System.out.println("setUpBeforeClass Called");
        manager= (EUSLAMangerSQLJPA) SLAManagerFactory.getSLAManagerInstance();
        Assert.assertNotNull(manager);
        
        manager= (EUSLAMangerSQLJPA) SLAManagerFactory.getSLAManagerInstance("unit_test");
        Assert.assertNotNull(manager);
        
        ems  = (EUSLAMangerSQLJPA) SLAManagerFactory.getSLAManagerInstance("unit_test");
        Assert.assertNotNull(ems);
        counter++;
        System.out.println("counter vale: "+counter);
        
        id = manager.create(new SLADocument(readFile("src/test/resources/sla_example.xml")));
        Assert.assertNotNull(id);
        
    }
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        //do nothing
    }

    /*
     * This is a support method that allows to create a new SLA and di increase the counter.
     */
    private void createSla(){
        id = manager.create(new SLADocument());
        counter++;
    }

    @Before
    public void setUp() throws Exception {
       // System.out.println("setUp Called");
        Assert.assertNotNull(manager);
    }
    //
    //  @After
    //  public void tearDown() throws Exception {
    //      System.out.println("tearDown Called");
    //  }

    @Test
    public final void createTest() {
        Assert.assertNotNull(manager);
        createSla();
        Assert.assertEquals(manager.getState(id),SLASTATEenum.PENDING.toString());
        Assert.assertNotNull(id);
    }

    @Test
    public final void retrieveTest() {      
        SLADocument doc = manager.retrieve(id);
        Assert.assertNotNull(doc); 
    }

    @Test(expected=IllegalStateException.class)
    public final void observeNotSignedSlaTest() {   
        createSla();
        manager.observe(id); 
    }


    @Test
    public final void observeSignedSlaTest() {
        manager.sign(id, manager.retrieve(id), null);
        manager.observe(id);
        Assert.assertEquals(manager.getState(id), SLASTATEenum.OBSERVED.toString());
    }

    @Test
    public final void signTest() {
        createSla();
        manager.sign(id, manager.retrieve(id), null);
        Assert.assertEquals(manager.getState(id), SLASTATEenum.SIGNED.toString()); 
    }



    @Test
    public final void testTerminate() {
        createSla();
        manager.sign(id, manager.retrieve(id), null);
        Assert.assertEquals(manager.getState(id), SLASTATEenum.SIGNED.toString()); 

        manager.observe(id);
        Assert.assertEquals(manager.getState(id), SLASTATEenum.OBSERVED.toString());

        manager.terminate(id);
        Assert.assertEquals(manager.getState(id), SLASTATEenum.TERMINATED.toString());
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    
    public final void testTerminateNotSignedSla() {
        createSla();    
        manager.sign(id, manager.retrieve(id), null);
        manager.terminate(id);
        thrown.expect(IllegalArgumentException.class);
    }
    
    
    @Test
    public final void testSignalAlert(){
        Assert.assertNotNull(manager);
        createSla();
        Assert.assertNotNull(manager.retrieve(id));
        manager.sign(id, manager.retrieve(id),null);
        Assert.assertEquals(manager.getState(id),SLASTATEenum.SIGNED.toString());
        
        manager.observe(id);
        Assert.assertEquals(manager.getState(id),SLASTATEenum.OBSERVED.toString());
        
        manager.signalAlert(id);
        Assert.assertEquals(manager.getState(id),SLASTATEenum.ALERTED.toString());
    }
    
    
    
    @Test
    public final void testSignalViolation(){
        Assert.assertNotNull(manager);
        createSla();
        Assert.assertNotNull(manager.retrieve(id));
        manager.sign(id, manager.retrieve(id),null);
        Assert.assertEquals(manager.getState(id),SLASTATEenum.SIGNED.toString());
        manager.observe(id);
        Assert.assertEquals(manager.getState(id),SLASTATEenum.OBSERVED.toString());
        manager.reNegotiate(id);
        Assert.assertEquals(manager.getState(id), SLASTATEenum.RENEGOTIATING.toString());
    }
    
    @Test
    public final void testComplete(){
        Assert.assertNotNull(manager);
        createSla();
        Assert.assertNotNull(manager.retrieve(id));
        manager.sign(id, manager.retrieve(id),null);
        Assert.assertEquals(manager.getState(id),SLASTATEenum.SIGNED.toString());
        manager.observe(id);
        Assert.assertEquals(manager.getState(id),SLASTATEenum.OBSERVED.toString());
        manager.complete(id);
        Assert.assertEquals(manager.getState(id), SLASTATEenum.COMPLETED.toString());
    }
    

    @Test
    public final void testGetState(){
        Assert.assertNotNull(manager);
        createSla();
        Assert.assertNotNull(manager.getState(id));
    }
    
    @Test
    public final void testGetAnnotations(){
        Assert.assertNotNull(manager);
        createSla();
        Assert.assertTrue(manager.getAnnotations(id).isEmpty());
        Annotation a = new Annotation();
        manager.annotate(id, a);
        Assert.assertFalse(manager.getAnnotations(id).isEmpty());
        Assert.assertNotNull(manager.getAnnotations(id));
    }
    
    
    @Test
    public final void testSearch(){
        Assert.assertNotNull(manager);
        createSla();
        Assert.assertNotNull(manager.search(null, null, 0, 1));
    }
    
    
    @Test
    public void testRetrieveAndLock(){
        Assert.assertNotNull(manager);
        createSla();
        Assert.assertEquals(manager.getState(id),SLASTATEenum.PENDING.toString());
        Assert.assertNotNull(manager.retrieveAndLock(id));
    }
    
    
    @Test
    public void testUpdate(){
        Assert.assertNotNull(manager);
        createSla();
        Assert.assertEquals(manager.getState(id),SLASTATEenum.PENDING.toString());
        Assert.assertNotNull(manager.retrieve(id));
        manager.update(id, manager.retrieve(id), null);
        Assert.assertEquals(manager.getState(id),SLASTATEenum.PENDING.toString());
    }
    
    
    
    @Test
    public void testUnlock(){
        Assert.assertNotNull(manager);
        createSla();
        manager.unlock(id, null);
        Assert.assertTrue(true);
    }
    
    
    @Test
    public void testUpdateAnnotations(){
        Assert.assertNotNull(manager);
        createSla();
        manager.updateAnnotation(id, null, null);
        Assert.assertTrue(true);
    }
    
    @Test(expected= IllegalArgumentException.class)
    public void testAnnotateException(){
        Assert.assertNotNull(manager);
        createSla();
        manager.annotate(id, null);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testUpdateException(){
        Assert.assertNotNull(manager);
        createSla();
        Assert.assertNotNull(manager.retrieve(id));
        manager.update(id, null, null);
    }
    
    @Test(expected= IllegalArgumentException.class)
    public void testCreateException(){
        Assert.assertNotNull(manager);
        createSla();
        manager.create(null);
    }
    
    
    
    @Test
    public void testSLADocument(){ 
        SLADocument doc = new SLADocument("xml1");
        Assert.assertNotNull(doc);
        Assert.assertEquals(doc.getSlaXmlDocument(),"xml1");
        doc.setSlaXmlDocument("xml2");
        Assert.assertEquals(doc.getSlaXmlDocument(),"xml2");
    }
    
    
    // testa Lock.java
    @Test
    public void testLock(){
        Lock l = new Lock();
        Date timestamp = new Date();
        Assert.assertEquals(l.hashCode(),0);
        Assert.assertTrue(l.equals(l));
        Assert.assertFalse(l.equals(null));
        Assert.assertFalse(l.equals(Lock.class));
//      Assert.assertEquals(l.getTimestamp(),timestamp);
    }

    
    
    @Test (expected= IllegalArgumentException.class)
    public void testPersistenceGetbyId(){
        ems.persistenceGetByID(null);
    }
    
    @Test (expected= IllegalArgumentException.class)
    public void testPersistenceGetbyIdNotValid(){
        id = new SLAIdentifier("0");
        ems.persistenceGetByID(id);
    }
    
    @Test (expected= IllegalArgumentException.class)
    public void testPersistenceCreate(){
        ems.persistenceCreate(null);
    }
    
    @Test 
    public void testPersistenceReleaseLock(){
        Lock lock = new Lock();
        Assert.assertFalse(ems.persistenceReleaseLock(id, lock));
    }
    
    @Test
    public void unMarshalTest(){
        JSONentityBuilder builder = new JSONentityBuilder ();
        MarshallingInterface inf = builder.unmarshal("", MarshallingInterface.class);
        Assert.assertNull(inf);
    }
    
    @Test
    public void marshalTest(){
        JSONmarshaller marshaller = new JSONmarshaller();
        MarshallingInterface inf = null;
        String marshString = marshaller.marshal(inf, MarshallingInterface.class);
        Assert.assertNotNull(marshString);
        
    }
    
    @Test
    public void XMLunMarshalTest(){
        XMLentityBuilder builder = new XMLentityBuilder();
        MarshallingInterface inf = builder.unmarshal("XMLmarshallerTest", MarshallingInterface.class);
        Assert.assertNull(inf);
    }
    
    @Test
    public void XMLmarshalTest(){
        XMLmarshaller marshaller = new XMLmarshaller();
        MarshallingInterface inf = null;
        String marshString = marshaller.marshal(inf, MarshallingInterface.class);
        Assert.assertNull(marshString);
    }
    
    @Test
    public void testCollection(){
        String resource = "resource";
        int total = 1;
        int members = 2;
        ArrayList<Item> items = new ArrayList<Item>();
        Collection c = new Collection();
        Collection c1 = new Collection(resource,total,members,(java.util.List<Item>) items);
        Assert.assertNotNull(c);
        Assert.assertNotNull(c1);
    }
    
    @Test
    public void testItem(){
        String id = "1";
        String itemValue = "itemValue";
        Item i = new Item();
        Item i1 = new Item(id,itemValue);
        Assert.assertNotNull(i);
        Assert.assertNotNull(i1);
    }
    
    public static String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }
}
