package eu.specsproject.slaplatform.slamanager.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import eu.specsproject.slaplatform.slamanager.SLAManager;
import eu.specsproject.slaplatform.slamanager.SLAManagerFactory;
import eu.specsproject.slaplatform.slamanager.entities.SLADocument;
import eu.specsproject.slaplatform.slamanager.internal.EUSLAMangerSQLJPA;

public class SLAManagerTestEntities {
	
	private static SLAManager manager;
	
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("setUpBeforeClass Called");
        manager= (EUSLAMangerSQLJPA) SLAManagerFactory.getSLAManagerInstance();
        Assert.assertNotNull(manager);
    }
    
    @Before
    public void setUp() throws Exception {
        System.out.println("setUp Called");
        Assert.assertNotNull(manager);
    }
    
    @Test
    public void testSLADocument(){
    	Assert.assertNotNull(manager);
    	SLADocument doc = new SLADocument("xml1");
    	Assert.assertNotNull(doc);
    	Assert.assertEquals(doc.getSlaXmlDocument(),"xml1");
    	doc.setSlaXmlDocument("xml2");
    	Assert.assertEquals(doc.getSlaXmlDocument(),"xml2");
    }
}
